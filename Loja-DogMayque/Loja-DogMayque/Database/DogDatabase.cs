﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMayque.Database
{
    class DogDatabase
    {
        public List<Modelos.DogModelo> Listar()
        {
            string script = "select * from tb_racao ";

            List<MySqlParameter> Par = new List<MySqlParameter>();

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, Par);

            List<Modelos.DogModelo> lista = new List<Modelos.DogModelo>();

            while (reader.Read())
            {
                Modelos.DogModelo racao = new Modelos.DogModelo();
                racao.ID = Convert.ToInt32(reader["id_racao"]);
                racao.Nome = Convert.ToString(reader["nm_racao"]);
                racao.Tipo = Convert.ToString(reader["ds_tipo"]);
                racao.qtd = Convert.ToInt32(reader["qtd_racao"]);
                racao.preco = Convert.ToDecimal(reader["vl_preco"]);

                lista.Add(racao);
            }
            reader.Close();

            return lista;
        }

        public List<Modelos.DogModelo> Filtrar(string nome)
        {
            string script = "select * from tb_racao where nm_racao like @nm_racao";

            List<MySqlParameter> Par = new List<MySqlParameter>();
            Par.Add(new MySqlParameter("nm_racao", nome));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, Par);

            List<Modelos.DogModelo> lista = new List<Modelos.DogModelo>();

            while (reader.Read())
            {
                Modelos.DogModelo racao = new Modelos.DogModelo();
                racao.ID = Convert.ToInt32(reader["id_racao"]);
                racao.Nome = Convert.ToString(reader["nm_racao"]);
                racao.Tipo = Convert.ToString(reader["ds_tipo"]);
                racao.qtd = Convert.ToInt32(reader["qtd_racao"]);
                racao.preco = Convert.ToDecimal(reader["vl_preco"]);

                lista.Add(racao);
            }
            reader.Close();

            return lista;
        }
        public void Cadastrar(Modelos.DogModelo dog)
        {
            string script = @"Insert into tb_racao(nm_racao,ds_tipo,qtd_racao,vl_preco)
                                             values(@nm_racao,@ds_tipo,@qtd_racao,@vl_preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_racao", dog.Nome));
            parms.Add(new MySqlParameter("ds_tipo", dog.Tipo));
            parms.Add(new MySqlParameter("qtd_racao", dog.qtd));
            parms.Add(new MySqlParameter("vl_preco", dog.preco));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);

        }
        public void Deletar(int id)
        {
            string script = @"delete from tb_racao 
                                     where id_racao = @id";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id", id));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);

        }
        public Modelos.DogModelo BuscarId(int id)
        {
            string script = "select * from tb_racao where id_racao = @id";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id", id));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            Modelos.DogModelo dog = null;

            if (reader.Read())
            {
                dog = new Modelos.DogModelo();
                dog.ID = Convert.ToInt32(reader["id_racao"]);
                dog.Nome = Convert.ToString(reader["nm_racao"]);
                dog.Tipo = Convert.ToString(reader["ds_tipo"]);
                dog.qtd = Convert.ToInt32(reader["qtd_racao"]);
                dog.preco = Convert.ToDecimal(reader["vl_preco"]);

            }
            reader.Close();

            return dog;

        }
        public void Alterar(Modelos.DogModelo dog)
        {
            string script = @"update tb_racao 
                                     set nm_racao = @nome,
                                        qtd_racao = @qtd,
                                         vl_preco = @preco
                                 where id_racao = @id";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id", dog.ID));
            parms.Add(new MySqlParameter("nome", dog.Nome));
            parms.Add(new MySqlParameter("qtd", dog.qtd));
            parms.Add(new MySqlParameter("preco", dog.preco));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);

        }
        public void AlterarQtd(int qtdR, int qtdV)
        {

        }



    }
}
