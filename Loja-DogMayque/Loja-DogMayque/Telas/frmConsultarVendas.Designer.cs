﻿namespace Loja_DogMayque.Telas
{
    partial class frmConsultarVendas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpComeco = new System.Windows.Forms.DateTimePicker();
            this.dgvVendas = new System.Windows.Forms.DataGridView();
            this.dtpFinal = new System.Windows.Forms.DateTimePicker();
            this.imgFechar = new System.Windows.Forms.PictureBox();
            this.imgCalcular = new System.Windows.Forms.PictureBox();
            this.lblPreco = new System.Windows.Forms.Label();
            this.imgMinizar = new System.Windows.Forms.PictureBox();
            this.imgVoltar = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFechar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCalcular)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMinizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVoltar)).BeginInit();
            this.SuspendLayout();
            // 
            // dtpComeco
            // 
            this.dtpComeco.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.dtpComeco.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dtpComeco.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpComeco.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpComeco.Location = new System.Drawing.Point(58, 83);
            this.dtpComeco.Name = "dtpComeco";
            this.dtpComeco.Size = new System.Drawing.Size(168, 20);
            this.dtpComeco.TabIndex = 0;
            this.dtpComeco.ValueChanged += new System.EventHandler(this.dtpComeco_ValueChanged);
            // 
            // dgvVendas
            // 
            this.dgvVendas.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvVendas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvVendas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVendas.Location = new System.Drawing.Point(31, 133);
            this.dgvVendas.Name = "dgvVendas";
            this.dgvVendas.ReadOnly = true;
            this.dgvVendas.Size = new System.Drawing.Size(541, 179);
            this.dgvVendas.TabIndex = 3;
            // 
            // dtpFinal
            // 
            this.dtpFinal.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.dtpFinal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dtpFinal.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFinal.Location = new System.Drawing.Point(381, 83);
            this.dtpFinal.Name = "dtpFinal";
            this.dtpFinal.Size = new System.Drawing.Size(168, 20);
            this.dtpFinal.TabIndex = 4;
            this.dtpFinal.ValueChanged += new System.EventHandler(this.dtpFinal_ValueChanged);
            // 
            // imgFechar
            // 
            this.imgFechar.BackColor = System.Drawing.Color.Transparent;
            this.imgFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgFechar.Location = new System.Drawing.Point(562, 12);
            this.imgFechar.Name = "imgFechar";
            this.imgFechar.Size = new System.Drawing.Size(30, 22);
            this.imgFechar.TabIndex = 11;
            this.imgFechar.TabStop = false;
            this.imgFechar.Click += new System.EventHandler(this.imgFechar_Click);
            // 
            // imgCalcular
            // 
            this.imgCalcular.BackColor = System.Drawing.Color.Transparent;
            this.imgCalcular.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgCalcular.Location = new System.Drawing.Point(73, 331);
            this.imgCalcular.Name = "imgCalcular";
            this.imgCalcular.Size = new System.Drawing.Size(174, 40);
            this.imgCalcular.TabIndex = 12;
            this.imgCalcular.TabStop = false;
            this.imgCalcular.Click += new System.EventHandler(this.imgCalcular_Click);
            // 
            // lblPreco
            // 
            this.lblPreco.AutoSize = true;
            this.lblPreco.BackColor = System.Drawing.Color.Transparent;
            this.lblPreco.Location = new System.Drawing.Point(280, 339);
            this.lblPreco.Name = "lblPreco";
            this.lblPreco.Size = new System.Drawing.Size(10, 13);
            this.lblPreco.TabIndex = 13;
            this.lblPreco.Text = "-";
            // 
            // imgMinizar
            // 
            this.imgMinizar.BackColor = System.Drawing.Color.Transparent;
            this.imgMinizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgMinizar.Location = new System.Drawing.Point(515, 12);
            this.imgMinizar.Name = "imgMinizar";
            this.imgMinizar.Size = new System.Drawing.Size(41, 22);
            this.imgMinizar.TabIndex = 14;
            this.imgMinizar.TabStop = false;
            this.imgMinizar.Click += new System.EventHandler(this.imgMinizar_Click);
            // 
            // imgVoltar
            // 
            this.imgVoltar.BackColor = System.Drawing.Color.Transparent;
            this.imgVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgVoltar.Location = new System.Drawing.Point(1, 1);
            this.imgVoltar.Name = "imgVoltar";
            this.imgVoltar.Size = new System.Drawing.Size(55, 48);
            this.imgVoltar.TabIndex = 15;
            this.imgVoltar.TabStop = false;
            this.imgVoltar.Click += new System.EventHandler(this.imgVoltar_Click);
            // 
            // frmConsultarVendas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Loja_DogMayque.Properties.Resources.Consultar_Vendas_Dog_Mayque2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(604, 383);
            this.Controls.Add(this.imgVoltar);
            this.Controls.Add(this.imgMinizar);
            this.Controls.Add(this.lblPreco);
            this.Controls.Add(this.imgCalcular);
            this.Controls.Add(this.imgFechar);
            this.Controls.Add(this.dtpFinal);
            this.Controls.Add(this.dgvVendas);
            this.Controls.Add(this.dtpComeco);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmConsultarVendas";
            this.Text = "ConsultarV";
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFechar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCalcular)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMinizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVoltar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpComeco;
        private System.Windows.Forms.DataGridView dgvVendas;
        private System.Windows.Forms.DateTimePicker dtpFinal;
        private System.Windows.Forms.PictureBox imgFechar;
        private System.Windows.Forms.PictureBox imgCalcular;
        private System.Windows.Forms.Label lblPreco;
        private System.Windows.Forms.PictureBox imgMinizar;
        private System.Windows.Forms.PictureBox imgVoltar;
    }
}