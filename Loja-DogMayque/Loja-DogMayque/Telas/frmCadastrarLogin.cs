﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Loja_DogMayque.Telas
{
    public partial class frmCadastrarLogin : Form
    {
        public frmCadastrarLogin()
        {
            InitializeComponent();
        }

        private void imgVoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void imgFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void imgCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                DogMayque.Modelos.LoginModelo login = new DogMayque.Modelos.LoginModelo();
                login.User = txtNome.Text;
                login.Senha = txtSenha.Text;

                DogMayque.Business.LoginBusiness business = new DogMayque.Business.LoginBusiness();
                business.Cadastrar(login);

                MessageBox.Show("Usuario cadastrado com sucesso");
            }
            catch (Exception)
            {

                MessageBox.Show("Desculpe, ocorreu um erro", "DogMayque", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);

            }
        }

        private void imgMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void imgVoltar_Click_1(object sender, EventArgs e)
        {
            frmLogin tela = new frmLogin();
            tela.Show();
            this.Hide();
        }
        public const int WM_NCLBUTTONDOWN = 0xA1;

        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]

        public static extern int SendMessage(IntPtr hWnd,

        int Msg, int wParam, int lParam);

        [DllImportAttribute("user32.dll")]

        public static extern bool ReleaseCapture();
    }
}
