﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Loja_DogMayque.Telas
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void ImgFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ImgMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void FrmMenu_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        public const int WM_NCLBUTTONDOWN = 0xA1;

        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]

        public static extern int SendMessage(IntPtr hWnd,

        int Msg, int wParam, int lParam);

        [DllImportAttribute("user32.dll")]

        public static extern bool ReleaseCapture();

        private void imgConsultarVendas_Click(object sender, EventArgs e)
        {
            frmConsultarVendas tela = new frmConsultarVendas();
            tela.Show();
            this.Hide();
        }

        private void imgCadastrarVendas_Click(object sender, EventArgs e)
        {
            frmCadastrarV tela = new frmCadastrarV();
            tela.Show();
            this.Hide();
        }

        private void imgDeletar_Click(object sender, EventArgs e)
        {
            frmDeletar tela = new frmDeletar();
            tela.Show();
            this.Hide();
        }

        private void imgAlterar_Click(object sender, EventArgs e)
        {
            frmAlterar tela = new frmAlterar();
            tela.Show();
            this.Hide();
        }

        private void imgConsultarRacao_Click(object sender, EventArgs e)
        {
            frmConsultarR tela = new frmConsultarR();
            tela.Show();
            this.Hide();
        }

        private void imgCadastrarRacao_Click(object sender, EventArgs e)
        {
            frmCadastrarR tela = new frmCadastrarR();
            tela.Show();
            this.Hide();
        }

        private void imgVoltar_Click(object sender, EventArgs e)
        {
            frmLogin tela = new frmLogin();
            tela.Show();
            this.Hide();
        }
    }
}
