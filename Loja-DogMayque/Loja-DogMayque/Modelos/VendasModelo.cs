﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMayque.Modelos
{
    class VendasModelo
    {
        public int IdVenda { get; set; }
        public int IdRacao { get; set; }
        public string nmRacao { get; set; }
        public int Qtd { get; set; }
        public decimal Preco { get; set; }
        public DateTime DataV { get; set; }
    }
}
