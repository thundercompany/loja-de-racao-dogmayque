﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Loja_DogMayque.Telas
{
    public partial class frmCadastrarR : Form
    {
        public frmCadastrarR()
        {
            InitializeComponent();

           
        }

        private void lblCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                DogMayque.Modelos.DogModelo dog = new DogMayque.Modelos.DogModelo();
                dog.Nome = txtNome.Text;
                dog.Tipo = txtTipo.Text;
                dog.qtd = Convert.ToInt32(nudQtd.Value);
                dog.preco = nudPreco.Value;

                DogMayque.Business.DogBusiness business = new DogMayque.Business.DogBusiness();
                business.Cadastrar(dog);

                MessageBox.Show("Ração cadastrada com sucesso");
            }
            catch (Exception)
            {

                MessageBox.Show("Desculpe, ocorreu um erro", "DogMayque", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);

            }

        }

        private void imgVoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void imgFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void imgMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        public const int WM_NCLBUTTONDOWN = 0xA1;

        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]

        public static extern int SendMessage(IntPtr hWnd,

        int Msg, int wParam, int lParam);

        [DllImportAttribute("user32.dll")]

        public static extern bool ReleaseCapture();
    }
   
}
