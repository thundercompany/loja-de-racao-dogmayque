﻿namespace Loja_DogMayque.Telas
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.btnFechar = new System.Windows.Forms.PictureBox();
            this.imgMinimizar = new System.Windows.Forms.PictureBox();
            this.imgEntrar = new System.Windows.Forms.PictureBox();
            this.imgCadastrar = new System.Windows.Forms.PictureBox();
            this.btnEntrar = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.btnFechar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEntrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCadastrar)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNome
            // 
            this.txtNome.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.Location = new System.Drawing.Point(65, 138);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(173, 19);
            this.txtNome.TabIndex = 0;
            this.toolTip1.SetToolTip(this.txtNome, "User");
            // 
            // txtSenha
            // 
            this.txtSenha.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSenha.Location = new System.Drawing.Point(65, 179);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Size = new System.Drawing.Size(173, 19);
            this.txtSenha.TabIndex = 1;
            this.toolTip1.SetToolTip(this.txtSenha, "Senha");
            this.txtSenha.UseSystemPasswordChar = true;
            // 
            // btnFechar
            // 
            this.btnFechar.BackColor = System.Drawing.Color.Transparent;
            this.btnFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFechar.Image = global::Loja_DogMayque.Properties.Resources.X_Dog_Mayque;
            this.btnFechar.Location = new System.Drawing.Point(261, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(37, 32);
            this.btnFechar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnFechar.TabIndex = 2;
            this.btnFechar.TabStop = false;
            this.toolTip1.SetToolTip(this.btnFechar, "Fechar");
            this.btnFechar.Click += new System.EventHandler(this.ImgFechar_Click);
            // 
            // imgMinimizar
            // 
            this.imgMinimizar.BackColor = System.Drawing.Color.Transparent;
            this.imgMinimizar.BackgroundImage = global::Loja_DogMayque.Properties.Resources.Minimizar_Dog_Mayque;
            this.imgMinimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgMinimizar.Location = new System.Drawing.Point(218, 24);
            this.imgMinimizar.Name = "imgMinimizar";
            this.imgMinimizar.Size = new System.Drawing.Size(37, 11);
            this.imgMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgMinimizar.TabIndex = 3;
            this.imgMinimizar.TabStop = false;
            this.toolTip1.SetToolTip(this.imgMinimizar, "Minimizar");
            this.imgMinimizar.Click += new System.EventHandler(this.ImgMinimizar_Click);
            // 
            // imgEntrar
            // 
            this.imgEntrar.BackColor = System.Drawing.Color.Transparent;
            this.imgEntrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgEntrar.Image = global::Loja_DogMayque.Properties.Resources.Botão_Dog_Mayque;
            this.imgEntrar.Location = new System.Drawing.Point(109, 216);
            this.imgEntrar.Name = "imgEntrar";
            this.imgEntrar.Size = new System.Drawing.Size(80, 35);
            this.imgEntrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgEntrar.TabIndex = 4;
            this.imgEntrar.TabStop = false;
            this.toolTip1.SetToolTip(this.imgEntrar, "Entrar");
            this.imgEntrar.Click += new System.EventHandler(this.ImgEntrar_Click);
            // 
            // imgCadastrar
            // 
            this.imgCadastrar.BackColor = System.Drawing.Color.Transparent;
            this.imgCadastrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgCadastrar.Location = new System.Drawing.Point(125, 280);
            this.imgCadastrar.Name = "imgCadastrar";
            this.imgCadastrar.Size = new System.Drawing.Size(164, 19);
            this.imgCadastrar.TabIndex = 5;
            this.imgCadastrar.TabStop = false;
            this.imgCadastrar.Click += new System.EventHandler(this.imgCadastrar_Click);
            // 
            // btnEntrar
            // 
            this.btnEntrar.Location = new System.Drawing.Point(-1, 289);
            this.btnEntrar.Name = "btnEntrar";
            this.btnEntrar.Size = new System.Drawing.Size(10, 10);
            this.btnEntrar.TabIndex = 6;
            this.btnEntrar.UseVisualStyleBackColor = true;
            this.btnEntrar.Visible = false;
            // 
            // frmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Loja_DogMayque.Properties.Resources.Login_Dog_Mayque;
            this.ClientSize = new System.Drawing.Size(301, 300);
            this.Controls.Add(this.btnEntrar);
            this.Controls.Add(this.imgCadastrar);
            this.Controls.Add(this.imgEntrar);
            this.Controls.Add(this.imgMinimizar);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.txtSenha);
            this.Controls.Add(this.txtNome);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmLogin";
            this.Text = "frmLogin";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmLogin_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.btnFechar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEntrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCadastrar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.PictureBox btnFechar;
        private System.Windows.Forms.PictureBox imgMinimizar;
        private System.Windows.Forms.PictureBox imgEntrar;
        private System.Windows.Forms.PictureBox imgCadastrar;
        private System.Windows.Forms.Button btnEntrar;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}