﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMayque.Business
{
    class VendasBusiness
    {
        public void Cadastrar(Modelos.VendasModelo vendas)
        {
            if (vendas.Qtd == 0)
                throw new ArgumentException("Coloque a quantidade");

            Database.VendasDatabase database = new Database.VendasDatabase();
            database.Cadastrar(vendas);
        }

        public List<Modelos.VendasModelo> FiltrarPorData(DateTime dataI, DateTime dataF)
        {
            Database.VendasDatabase vendasDatabase = new Database.VendasDatabase();
            List<Modelos.VendasModelo> lista = vendasDatabase.FiltrarPorData(dataI, dataF);

            return lista;
        }
        public decimal Calcular(DateTime dataI,DateTime dataF)
        {
            Database.VendasDatabase database = new Database.VendasDatabase();
            decimal preco= database.Calcular(dataI, dataF);

            return preco;
        }

    }
}
