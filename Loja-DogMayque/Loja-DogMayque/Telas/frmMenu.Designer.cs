﻿namespace Loja_DogMayque.Telas
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.imgConsultarVendas = new System.Windows.Forms.PictureBox();
            this.imgDeletar = new System.Windows.Forms.PictureBox();
            this.imgConsultarRacao = new System.Windows.Forms.PictureBox();
            this.imgCadastrarRacao = new System.Windows.Forms.PictureBox();
            this.imgAlterar = new System.Windows.Forms.PictureBox();
            this.imgCadastrarVendas = new System.Windows.Forms.PictureBox();
            this.imgFechar = new System.Windows.Forms.PictureBox();
            this.imgMinimizar = new System.Windows.Forms.PictureBox();
            this.imgVoltar = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgConsultarVendas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDeletar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgConsultarRacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCadastrarRacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAlterar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCadastrarVendas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFechar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVoltar)).BeginInit();
            this.SuspendLayout();
            // 
            // imgConsultarVendas
            // 
            this.imgConsultarVendas.BackColor = System.Drawing.Color.Transparent;
            this.imgConsultarVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgConsultarVendas.Location = new System.Drawing.Point(44, 96);
            this.imgConsultarVendas.Name = "imgConsultarVendas";
            this.imgConsultarVendas.Size = new System.Drawing.Size(247, 71);
            this.imgConsultarVendas.TabIndex = 0;
            this.imgConsultarVendas.TabStop = false;
            this.imgConsultarVendas.Click += new System.EventHandler(this.imgConsultarVendas_Click);
            // 
            // imgDeletar
            // 
            this.imgDeletar.BackColor = System.Drawing.Color.Transparent;
            this.imgDeletar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgDeletar.Location = new System.Drawing.Point(44, 220);
            this.imgDeletar.Name = "imgDeletar";
            this.imgDeletar.Size = new System.Drawing.Size(247, 71);
            this.imgDeletar.TabIndex = 1;
            this.imgDeletar.TabStop = false;
            this.imgDeletar.Click += new System.EventHandler(this.imgDeletar_Click);
            // 
            // imgConsultarRacao
            // 
            this.imgConsultarRacao.BackColor = System.Drawing.Color.Transparent;
            this.imgConsultarRacao.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgConsultarRacao.Location = new System.Drawing.Point(44, 351);
            this.imgConsultarRacao.Name = "imgConsultarRacao";
            this.imgConsultarRacao.Size = new System.Drawing.Size(247, 71);
            this.imgConsultarRacao.TabIndex = 2;
            this.imgConsultarRacao.TabStop = false;
            this.imgConsultarRacao.Click += new System.EventHandler(this.imgConsultarRacao_Click);
            // 
            // imgCadastrarRacao
            // 
            this.imgCadastrarRacao.BackColor = System.Drawing.Color.Transparent;
            this.imgCadastrarRacao.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgCadastrarRacao.Location = new System.Drawing.Point(321, 351);
            this.imgCadastrarRacao.Name = "imgCadastrarRacao";
            this.imgCadastrarRacao.Size = new System.Drawing.Size(247, 71);
            this.imgCadastrarRacao.TabIndex = 3;
            this.imgCadastrarRacao.TabStop = false;
            this.imgCadastrarRacao.Click += new System.EventHandler(this.imgCadastrarRacao_Click);
            // 
            // imgAlterar
            // 
            this.imgAlterar.BackColor = System.Drawing.Color.Transparent;
            this.imgAlterar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgAlterar.Location = new System.Drawing.Point(321, 220);
            this.imgAlterar.Name = "imgAlterar";
            this.imgAlterar.Size = new System.Drawing.Size(247, 71);
            this.imgAlterar.TabIndex = 4;
            this.imgAlterar.TabStop = false;
            this.imgAlterar.Click += new System.EventHandler(this.imgAlterar_Click);
            // 
            // imgCadastrarVendas
            // 
            this.imgCadastrarVendas.BackColor = System.Drawing.Color.Transparent;
            this.imgCadastrarVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgCadastrarVendas.Location = new System.Drawing.Point(321, 96);
            this.imgCadastrarVendas.Name = "imgCadastrarVendas";
            this.imgCadastrarVendas.Size = new System.Drawing.Size(247, 71);
            this.imgCadastrarVendas.TabIndex = 5;
            this.imgCadastrarVendas.TabStop = false;
            this.imgCadastrarVendas.Click += new System.EventHandler(this.imgCadastrarVendas_Click);
            // 
            // imgFechar
            // 
            this.imgFechar.BackColor = System.Drawing.Color.Transparent;
            this.imgFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgFechar.Location = new System.Drawing.Point(587, 2);
            this.imgFechar.Name = "imgFechar";
            this.imgFechar.Size = new System.Drawing.Size(42, 37);
            this.imgFechar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgFechar.TabIndex = 6;
            this.imgFechar.TabStop = false;
            this.imgFechar.Click += new System.EventHandler(this.ImgFechar_Click);
            // 
            // imgMinimizar
            // 
            this.imgMinimizar.BackColor = System.Drawing.Color.Transparent;
            this.imgMinimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgMinimizar.Location = new System.Drawing.Point(547, 9);
            this.imgMinimizar.Name = "imgMinimizar";
            this.imgMinimizar.Size = new System.Drawing.Size(42, 15);
            this.imgMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgMinimizar.TabIndex = 7;
            this.imgMinimizar.TabStop = false;
            this.imgMinimizar.Click += new System.EventHandler(this.ImgMinimizar_Click);
            // 
            // imgVoltar
            // 
            this.imgVoltar.BackColor = System.Drawing.Color.Transparent;
            this.imgVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgVoltar.Location = new System.Drawing.Point(4, 2);
            this.imgVoltar.Name = "imgVoltar";
            this.imgVoltar.Size = new System.Drawing.Size(42, 37);
            this.imgVoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgVoltar.TabIndex = 8;
            this.imgVoltar.TabStop = false;
            this.imgVoltar.Click += new System.EventHandler(this.imgVoltar_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Loja_DogMayque.Properties.Resources.Menu_Dog_Mayque1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(627, 495);
            this.Controls.Add(this.imgVoltar);
            this.Controls.Add(this.imgMinimizar);
            this.Controls.Add(this.imgFechar);
            this.Controls.Add(this.imgCadastrarVendas);
            this.Controls.Add(this.imgAlterar);
            this.Controls.Add(this.imgCadastrarRacao);
            this.Controls.Add(this.imgConsultarRacao);
            this.Controls.Add(this.imgDeletar);
            this.Controls.Add(this.imgConsultarVendas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMenu";
            this.Text = "frmMenu";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmMenu_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.imgConsultarVendas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDeletar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgConsultarRacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCadastrarRacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAlterar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCadastrarVendas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFechar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVoltar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox imgConsultarVendas;
        private System.Windows.Forms.PictureBox imgDeletar;
        private System.Windows.Forms.PictureBox imgConsultarRacao;
        private System.Windows.Forms.PictureBox imgCadastrarRacao;
        private System.Windows.Forms.PictureBox imgAlterar;
        private System.Windows.Forms.PictureBox imgCadastrarVendas;
        private System.Windows.Forms.PictureBox imgFechar;
        private System.Windows.Forms.PictureBox imgMinimizar;
        private System.Windows.Forms.PictureBox imgVoltar;
    }
}