﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Loja_DogMayque.Telas
{
    public partial class frmDeletar : Form
    {
        public frmDeletar()
        {
            InitializeComponent();
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DogMayque.Modelos.DogModelo dog = new DogMayque.Modelos.DogModelo();

                int id = Convert.ToInt32(txtID.Text);

                DogMayque.Business.DogBusiness business = new DogMayque.Business.DogBusiness();
                dog = business.BuscarId(id);

                if (dog != null)
                {
                    txtNome.Text = dog.Nome;
                    txtTipo.Text = dog.Tipo;
                    nudPreco.Value = dog.preco;

                }
                else
                {
                    MessageBox.Show("Desculpe, ocorreu um erro", "DogMayque", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Desculpe, ocorreu um erro","DogMayque",MessageBoxButtons.OKCancel,MessageBoxIcon.Error);
            }
        }

        private void imgSim_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtID.Text);

            DogMayque.Business.DogBusiness business = new DogMayque.Business.DogBusiness();
            business.Deletar(id);
        }

        private void imgN_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void imgVoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void imgMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void imgFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        public const int WM_NCLBUTTONDOWN = 0xA1;

        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]

        public static extern int SendMessage(IntPtr hWnd,

        int Msg, int wParam, int lParam);

        [DllImportAttribute("user32.dll")]

        public static extern bool ReleaseCapture();
    }
}
