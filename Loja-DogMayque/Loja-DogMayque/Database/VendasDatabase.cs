﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMayque.Database
{
    class VendasDatabase
    {
        public void Cadastrar(Modelos.VendasModelo vendas)
        {
            string script = @"insert tb_vendas(id_racao,data_venda,qtd_venda,preco_venda)
                                     values(@id_racao,@data_venda,@qtd_venda,@preco_venda)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_racao", vendas.IdRacao));
            parms.Add(new MySqlParameter("data_venda", vendas.DataV));
            parms.Add(new MySqlParameter("qtd_venda", vendas.Qtd));
            parms.Add(new MySqlParameter("preco_venda", vendas.Preco));

            DB bd = new DB();
            bd.ExecuteInsertScript(script, parms);


        }
        public List<Modelos.VendasModelo> FiltrarPorData(DateTime dataInicial, DateTime dataFinal)
        {
            string script = @"select id_venda , nm_racao,qtd_venda, preco_venda,data_venda
                              from tb_racao, tb_vendas
                              where data_venda > @data_Inicial and data_venda < @data_Final ";

            List<MySqlParameter> Par = new List<MySqlParameter>();
            Par.Add(new MySqlParameter("data_Inicial", dataInicial));
            Par.Add(new MySqlParameter("data_Final", dataFinal));


            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, Par);

            List<Modelos.VendasModelo> lista = new List<Modelos.VendasModelo>();

            while (reader.Read())
            {
                Modelos.VendasModelo vendas = new Modelos.VendasModelo();
                vendas.nmRacao = Convert.ToString(reader["nm_racao"]);
                vendas.IdVenda = Convert.ToInt32(reader["id_venda"]);
                vendas.IdRacao = Convert.ToInt32(reader["id_racao"]);
                vendas.DataV = Convert.ToDateTime(reader["data_venda"]);
                vendas.Qtd = Convert.ToInt32(reader["qtd_venda"]);
                vendas.Preco = Convert.ToDecimal(reader["preco_venda"]);

                lista.Add(vendas);
            }
            reader.Close();

            return lista;


        }
        public decimal Calcular(DateTime dataInicial, DateTime dataFinal)
        {
            string script = @"select preco_venda
                              from tb_racao, tb_vendas
                              where data_venda > @data_Inicial and data_venda < @data_Final ";

            List<MySqlParameter> Par = new List<MySqlParameter>();
            Par.Add(new MySqlParameter("data_Inicial", dataInicial));
            Par.Add(new MySqlParameter("data_Final", dataFinal));


            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, Par);

            List<Modelos.VendasModelo> lista = new List<Modelos.VendasModelo>();


            decimal total = 0;

            while(reader.Read())
            {
                    total += Convert.ToDecimal(reader["preco_venda"]);
            }
            reader.Close();



            return total ;

        }

    }
   
}