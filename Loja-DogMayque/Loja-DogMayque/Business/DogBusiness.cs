﻿using DogMayque.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMayque.Business
{
    class DogBusiness
    {

        public List<Modelos.DogModelo> Listar()
        {
            Database.DogDatabase consultarDatabase = new Database.DogDatabase();
            List<Modelos.DogModelo> lista = consultarDatabase.Listar();

            return lista;
        }

        public List<Modelos.DogModelo> Filtrar(string nome)
        {
            Database.DogDatabase consultarDatabase = new Database.DogDatabase();
            List<Modelos.DogModelo> lista = consultarDatabase.Filtrar(nome);

            return lista;

        }

        public void Cadastrar(Modelos.DogModelo dog)
        {
            if (dog.Nome == string.Empty)
                throw new ArgumentException("Nome é obrigatorio!");
            if (dog.Tipo == string.Empty)
                throw new ArgumentException("Tipo é obrigatorio!");
            if (dog.qtd < 0)
                throw new ArgumentException("Quantidade maior que 0 é obrigatorio");
            if (dog.preco == decimal.Zero)
                throw new ArgumentException("Tipo é obrigatorio!");

            Database.DogDatabase DogDatabase = new Database.DogDatabase();
            DogDatabase.Cadastrar(dog);
        }

        public Modelos.DogModelo BuscarId(int id)
        {
            DogDatabase database = new DogDatabase();
            return database.BuscarId(id);

        }
        public void Deletar(int id)
        {
            DogDatabase database = new DogDatabase();
            database.Deletar(id);
        }
        public void Alterar(Modelos.DogModelo dog)
        {
            DogDatabase database = new DogDatabase();
            database.Alterar(dog);
        }
        public void Inserir(Modelos.DogModelo Dog)
        {
            if (Dog.Nome == string.Empty)
            {
                throw new ArgumentException("nome obrigatorio");
            }

            Database.DogDatabase dogDatabase = new DogDatabase();
            dogDatabase.Cadastrar(Dog);
        }
    }
}
