﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Loja_DogMayque.Telas
{
    public partial class frmCadastrarV : Form
    {
        public frmCadastrarV()
        {
            InitializeComponent();

            DogMayque.Business.DogBusiness bus = new DogMayque.Business.DogBusiness();
            List<DogMayque.Modelos.DogModelo> lista = bus.Listar();

            cboNmRacao.DisplayMember = nameof(DogMayque.Modelos.DogModelo.Nome);
            cboNmRacao.DataSource = lista;
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                DogMayque.Modelos.DogModelo DOG = cboNmRacao.SelectedItem as DogMayque.Modelos.DogModelo;

                DogMayque.Modelos.VendasModelo vendas = new DogMayque.Modelos.VendasModelo();
                vendas.IdRacao = DOG.ID;
                vendas.Preco = this.CalcularTotal();
                vendas.Qtd = Convert.ToInt32(nudQtd.Value);
                vendas.DataV = DateTime.Now;



                DogMayque.Business.VendasBusiness dogBusiness = new DogMayque.Business.VendasBusiness();
                dogBusiness.Cadastrar(vendas);

                MessageBox.Show("Cadastrado com sucesso");
            }
            catch (Exception)
            {

                MessageBox.Show("Desculpe, ocorreu um erro","DogMayque",MessageBoxButtons.OKCancel,MessageBoxIcon.Error);

            }
        }
        private decimal CalcularTotal()
        {
            
                DogMayque.Modelos.DogModelo d = cboNmRacao.SelectedItem as DogMayque.Modelos.DogModelo;

                int qtd = Convert.ToInt32(nudQtd.Value);
                decimal total = d.preco * qtd;

                return total;
            
        }

        private void nudQtd_ValueChanged_2(object sender, EventArgs e)
        {
            nudPreco.Value =this.CalcularTotal();

        }

        private void cboNmRacao_SelectedIndexChanged(object sender, EventArgs e)
        {
            DogMayque.Modelos.DogModelo d = cboNmRacao.SelectedItem as DogMayque.Modelos.DogModelo;

            nudPreco.Value = d.preco;
        }

        private void imgFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void imgVoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void imgMinizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        public const int WM_NCLBUTTONDOWN = 0xA1;

        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]

        public static extern int SendMessage(IntPtr hWnd,

        int Msg, int wParam, int lParam);

        [DllImportAttribute("user32.dll")]

        public static extern bool ReleaseCapture();
    }
}
