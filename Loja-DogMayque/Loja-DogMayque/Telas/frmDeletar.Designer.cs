﻿namespace Loja_DogMayque.Telas
{
    partial class frmDeletar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtID = new System.Windows.Forms.TextBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtTipo = new System.Windows.Forms.TextBox();
            this.nudPreco = new System.Windows.Forms.NumericUpDown();
            this.imgSim = new System.Windows.Forms.PictureBox();
            this.imgN = new System.Windows.Forms.PictureBox();
            this.imgMinimizar = new System.Windows.Forms.PictureBox();
            this.imgFechar = new System.Windows.Forms.PictureBox();
            this.imgVoltar = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudPreco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSim)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFechar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVoltar)).BeginInit();
            this.SuspendLayout();
            // 
            // txtID
            // 
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.Location = new System.Drawing.Point(134, 98);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(202, 19);
            this.txtID.TabIndex = 0;
            this.txtID.TextChanged += new System.EventHandler(this.txtID_TextChanged);
            // 
            // txtNome
            // 
            this.txtNome.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtNome.Location = new System.Drawing.Point(134, 163);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(214, 19);
            this.txtNome.TabIndex = 1;
            // 
            // txtTipo
            // 
            this.txtTipo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtTipo.Location = new System.Drawing.Point(134, 212);
            this.txtTipo.Name = "txtTipo";
            this.txtTipo.Size = new System.Drawing.Size(214, 19);
            this.txtTipo.TabIndex = 2;
            // 
            // nudPreco
            // 
            this.nudPreco.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nudPreco.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudPreco.Location = new System.Drawing.Point(134, 262);
            this.nudPreco.Name = "nudPreco";
            this.nudPreco.Size = new System.Drawing.Size(214, 16);
            this.nudPreco.TabIndex = 3;
            // 
            // imgSim
            // 
            this.imgSim.BackColor = System.Drawing.Color.Transparent;
            this.imgSim.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgSim.Location = new System.Drawing.Point(94, 323);
            this.imgSim.Name = "imgSim";
            this.imgSim.Size = new System.Drawing.Size(68, 40);
            this.imgSim.TabIndex = 4;
            this.imgSim.TabStop = false;
            this.imgSim.Click += new System.EventHandler(this.imgSim_Click);
            // 
            // imgN
            // 
            this.imgN.BackColor = System.Drawing.Color.Transparent;
            this.imgN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgN.Location = new System.Drawing.Point(220, 323);
            this.imgN.Name = "imgN";
            this.imgN.Size = new System.Drawing.Size(68, 40);
            this.imgN.TabIndex = 5;
            this.imgN.TabStop = false;
            this.imgN.Click += new System.EventHandler(this.imgN_Click);
            // 
            // imgMinimizar
            // 
            this.imgMinimizar.BackColor = System.Drawing.Color.Transparent;
            this.imgMinimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgMinimizar.Location = new System.Drawing.Point(285, 0);
            this.imgMinimizar.Name = "imgMinimizar";
            this.imgMinimizar.Size = new System.Drawing.Size(43, 40);
            this.imgMinimizar.TabIndex = 6;
            this.imgMinimizar.TabStop = false;
            this.imgMinimizar.Click += new System.EventHandler(this.imgMinimizar_Click);
            // 
            // imgFechar
            // 
            this.imgFechar.BackColor = System.Drawing.Color.Transparent;
            this.imgFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgFechar.Location = new System.Drawing.Point(332, 0);
            this.imgFechar.Name = "imgFechar";
            this.imgFechar.Size = new System.Drawing.Size(43, 40);
            this.imgFechar.TabIndex = 7;
            this.imgFechar.TabStop = false;
            this.imgFechar.Click += new System.EventHandler(this.imgFechar_Click);
            // 
            // imgVoltar
            // 
            this.imgVoltar.BackColor = System.Drawing.Color.Transparent;
            this.imgVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgVoltar.Location = new System.Drawing.Point(0, 0);
            this.imgVoltar.Name = "imgVoltar";
            this.imgVoltar.Size = new System.Drawing.Size(60, 51);
            this.imgVoltar.TabIndex = 8;
            this.imgVoltar.TabStop = false;
            this.imgVoltar.Click += new System.EventHandler(this.imgVoltar_Click);
            // 
            // frmDeletar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Loja_DogMayque.Properties.Resources.Deletar_Dog_Mayque;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(375, 370);
            this.Controls.Add(this.imgVoltar);
            this.Controls.Add(this.imgFechar);
            this.Controls.Add(this.imgMinimizar);
            this.Controls.Add(this.imgN);
            this.Controls.Add(this.imgSim);
            this.Controls.Add(this.nudPreco);
            this.Controls.Add(this.txtTipo);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.txtID);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmDeletar";
            this.Text = "frmDeletar";
            ((System.ComponentModel.ISupportInitialize)(this.nudPreco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSim)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFechar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVoltar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox txtTipo;
        private System.Windows.Forms.NumericUpDown nudPreco;
        private System.Windows.Forms.PictureBox imgSim;
        private System.Windows.Forms.PictureBox imgN;
        private System.Windows.Forms.PictureBox imgMinimizar;
        private System.Windows.Forms.PictureBox imgFechar;
        private System.Windows.Forms.PictureBox imgVoltar;
    }
}