﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMayque.Database
{
    class LoginDatabase
    {
        public void Cadastrar(Modelos.LoginModelo login)
        {
            string script = @"Insert into tb_login(nm_user,senha_user)
                                             values(@user,@senha)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("user", login.User));
            parms.Add(new MySqlParameter("senha", login.Senha));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);

        }

        public Modelos.LoginModelo VerificarLogin(string user, string senha)
        {
            string script = @"select * from tb_login where nm_user = @nm_user and senha_user = @senha_user";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_user", user));
            parms.Add(new MySqlParameter("senha_user", senha));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            Modelos.LoginModelo login = null;

            if (reader.Read())
            {
                login = new Modelos.LoginModelo();
                login.Id = Convert.ToInt32(reader["id_user"]);
                login.User = Convert.ToString(reader["nm_user"]);
                login.Senha = Convert.ToString(reader["senha_user"]);

            }
            reader.Close();
            return login;
        }


    }
}
