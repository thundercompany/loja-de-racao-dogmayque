﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMayque.Modelos
{
    class LoginModelo
    {
        public int Id { get; set; }
        public string User { get; set; }
        public string Senha { get; set; }
    }
}
