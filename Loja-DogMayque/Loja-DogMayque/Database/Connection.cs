﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMayque.Database
{
    class Connection
    {
        public MySqlConnection Create()
        {
            string connectionString = "server=localhost;database=Bd_DogMayque;uid=root";

            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            return connection;

            //;password=12345
        }
    }
}
