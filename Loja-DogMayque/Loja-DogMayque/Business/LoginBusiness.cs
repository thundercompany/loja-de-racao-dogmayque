﻿using DogMayque.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMayque.Business
{
    class LoginBusiness
    {
        public void Cadastrar(Modelos.LoginModelo login)
        {
            if (login.User == string.Empty)
                throw new ArgumentException("User é obrigatorio");
            if (login.Senha == string.Empty)
                throw new ArgumentException("Senha é obrigatorio");

            LoginDatabase database = new LoginDatabase();
            database.Cadastrar(login);
        }
        public Modelos.LoginModelo VerificarLogin(string user, string senha)
        {
            LoginDatabase database = new LoginDatabase();
            Modelos.LoginModelo login = database.VerificarLogin(user, senha);

            return login;

        }
    }
}
