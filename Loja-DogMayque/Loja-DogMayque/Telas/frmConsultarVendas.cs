﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Loja_DogMayque.Telas
{
    public partial class frmConsultarVendas : Form
    {
        public frmConsultarVendas()
        {
            InitializeComponent();
        }

        private void dtpFinal_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                dgvVendas.DataSource = this.Consultar();

            }
            catch (Exception)
            {
                MessageBox.Show("Desculpe, ocorreu um erro", "DogMayque", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }


        private List<DogMayque.Modelos.VendasModelo> Consultar()
        {
                DateTime dataI = dtpComeco.Value.Date;
                DateTime dataF = dtpFinal.Value.Date;

                DogMayque.Business.VendasBusiness business = new DogMayque.Business.VendasBusiness();
                List<DogMayque.Modelos.VendasModelo> lista = business.FiltrarPorData(dataI, dataF);

                return lista;
          
        }

        private void imgCalcular_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime dataI = dtpComeco.Value.Date;
                DateTime dataF = dtpFinal.Value.Date;

                DogMayque.Business.VendasBusiness business = new DogMayque.Business.VendasBusiness();
                decimal total = business.Calcular(dataI, dataF);

                lblPreco.Text = Convert.ToString(total);
            }
            catch (Exception)
            {
                MessageBox.Show("Desculpe, ocorreu um erro","DogMayque",MessageBoxButtons.OKCancel,MessageBoxIcon.Error);

            }

        }

        private void imgFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void imgMinizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;

        }

        private void imgVoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }
        public const int WM_NCLBUTTONDOWN = 0xA1;

        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]

        public static extern int SendMessage(IntPtr hWnd,

        int Msg, int wParam, int lParam);

        [DllImportAttribute("user32.dll")]

        public static extern bool ReleaseCapture();

        private void dtpComeco_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                dgvVendas.DataSource = this.Consultar();

            }
            catch (Exception)
            {

                MessageBox.Show("Desculpe, ocorreu um erro", "DogMayque", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);

            }

        }
    }
}
