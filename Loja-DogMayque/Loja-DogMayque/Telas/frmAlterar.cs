﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Loja_DogMayque.Telas
{
    public partial class frmAlterar : Form
    {
        public frmAlterar()
        {
            InitializeComponent();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                DogMayque.Modelos.DogModelo dog = new DogMayque.Modelos.DogModelo();

                dog.ID = Convert.ToInt32(txtId.Text);
                dog.Nome = txtNome.Text;
                dog.preco = nudPreco.Value;
                dog.Tipo = txtTipo.Text;
                dog.qtd = Convert.ToInt32(nudQtd.Value);


                DogMayque.Business.DogBusiness business = new DogMayque.Business.DogBusiness();
                business.Alterar(dog);

                MessageBox.Show("Registro alterado com sucesso", "DogMayque", MessageBoxButtons.OK);

            }
            catch (Exception)
            {
                MessageBox.Show("Desculpe, ocorreu um erro", "DogMayque", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);

            }
        }

        private void imgVoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtId_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        public const int WM_NCLBUTTONDOWN = 0xA1;

        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]

        public static extern int SendMessage(IntPtr hWnd,

        int Msg, int wParam, int lParam);

        [DllImportAttribute("user32.dll")]

        public static extern bool ReleaseCapture();
    }
}
