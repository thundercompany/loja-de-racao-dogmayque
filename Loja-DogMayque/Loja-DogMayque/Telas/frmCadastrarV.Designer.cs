﻿namespace Loja_DogMayque.Telas
{
    partial class frmCadastrarV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCadastrar = new System.Windows.Forms.PictureBox();
            this.nudPreco = new System.Windows.Forms.NumericUpDown();
            this.imgFechar = new System.Windows.Forms.PictureBox();
            this.imgMinizar = new System.Windows.Forms.PictureBox();
            this.imgVoltar = new System.Windows.Forms.PictureBox();
            this.cboNmRacao = new System.Windows.Forms.ComboBox();
            this.nudQtd = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.btnCadastrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPreco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFechar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMinizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVoltar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQtd)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCadastrar
            // 
            this.btnCadastrar.BackColor = System.Drawing.Color.Transparent;
            this.btnCadastrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCadastrar.Location = new System.Drawing.Point(457, 152);
            this.btnCadastrar.Name = "btnCadastrar";
            this.btnCadastrar.Size = new System.Drawing.Size(91, 106);
            this.btnCadastrar.TabIndex = 8;
            this.btnCadastrar.TabStop = false;
            this.btnCadastrar.Click += new System.EventHandler(this.btnCadastrar_Click);
            // 
            // nudPreco
            // 
            this.nudPreco.BackColor = System.Drawing.Color.White;
            this.nudPreco.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nudPreco.DecimalPlaces = 2;
            this.nudPreco.Location = new System.Drawing.Point(180, 244);
            this.nudPreco.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudPreco.Name = "nudPreco";
            this.nudPreco.Size = new System.Drawing.Size(217, 16);
            this.nudPreco.TabIndex = 9;
            this.nudPreco.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // imgFechar
            // 
            this.imgFechar.BackColor = System.Drawing.Color.Transparent;
            this.imgFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgFechar.Location = new System.Drawing.Point(555, 2);
            this.imgFechar.Name = "imgFechar";
            this.imgFechar.Size = new System.Drawing.Size(42, 40);
            this.imgFechar.TabIndex = 10;
            this.imgFechar.TabStop = false;
            this.imgFechar.Click += new System.EventHandler(this.imgFechar_Click);
            // 
            // imgMinizar
            // 
            this.imgMinizar.BackColor = System.Drawing.Color.Transparent;
            this.imgMinizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgMinizar.Location = new System.Drawing.Point(509, 2);
            this.imgMinizar.Name = "imgMinizar";
            this.imgMinizar.Size = new System.Drawing.Size(42, 40);
            this.imgMinizar.TabIndex = 11;
            this.imgMinizar.TabStop = false;
            this.imgMinizar.Click += new System.EventHandler(this.imgMinizar_Click);
            // 
            // imgVoltar
            // 
            this.imgVoltar.BackColor = System.Drawing.Color.Transparent;
            this.imgVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgVoltar.Location = new System.Drawing.Point(3, 2);
            this.imgVoltar.Name = "imgVoltar";
            this.imgVoltar.Size = new System.Drawing.Size(55, 48);
            this.imgVoltar.TabIndex = 12;
            this.imgVoltar.TabStop = false;
            this.imgVoltar.Click += new System.EventHandler(this.imgVoltar_Click);
            // 
            // cboNmRacao
            // 
            this.cboNmRacao.BackColor = System.Drawing.Color.White;
            this.cboNmRacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNmRacao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboNmRacao.FormattingEnabled = true;
            this.cboNmRacao.Location = new System.Drawing.Point(180, 147);
            this.cboNmRacao.Name = "cboNmRacao";
            this.cboNmRacao.Size = new System.Drawing.Size(217, 21);
            this.cboNmRacao.TabIndex = 13;
            this.cboNmRacao.SelectedIndexChanged += new System.EventHandler(this.cboNmRacao_SelectedIndexChanged);
            // 
            // nudQtd
            // 
            this.nudQtd.BackColor = System.Drawing.Color.White;
            this.nudQtd.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nudQtd.Location = new System.Drawing.Point(180, 195);
            this.nudQtd.Name = "nudQtd";
            this.nudQtd.Size = new System.Drawing.Size(217, 16);
            this.nudQtd.TabIndex = 14;
            this.nudQtd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudQtd.ValueChanged += new System.EventHandler(this.nudQtd_ValueChanged_2);
            // 
            // frmCadastrarV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Loja_DogMayque.Properties.Resources.Cadastrar_Vendas_Dog_Mayque__1_;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(599, 394);
            this.Controls.Add(this.nudQtd);
            this.Controls.Add(this.cboNmRacao);
            this.Controls.Add(this.imgVoltar);
            this.Controls.Add(this.imgMinizar);
            this.Controls.Add(this.imgFechar);
            this.Controls.Add(this.nudPreco);
            this.Controls.Add(this.btnCadastrar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmCadastrarV";
            this.Text = "frmCadastrarV";
            ((System.ComponentModel.ISupportInitialize)(this.btnCadastrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPreco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFechar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMinizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVoltar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQtd)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox btnCadastrar;
        private System.Windows.Forms.NumericUpDown nudPreco;
        private System.Windows.Forms.PictureBox imgFechar;
        private System.Windows.Forms.PictureBox imgMinizar;
        private System.Windows.Forms.PictureBox imgVoltar;
        private System.Windows.Forms.ComboBox cboNmRacao;
        private System.Windows.Forms.NumericUpDown nudQtd;
    }
}