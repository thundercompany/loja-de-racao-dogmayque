﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Loja_DogMayque.Telas
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void ImgMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void FrmLogin_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        public const int WM_NCLBUTTONDOWN = 0xA1;

        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]

        public static extern int SendMessage(IntPtr hWnd,

        int Msg, int wParam, int lParam);

        [DllImportAttribute("user32.dll")]

        public static extern bool ReleaseCapture();

        private void ImgFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ImgEntrar_Click(object sender, EventArgs e)
        {
            this.Login();
        }
        private void Login()
        {

            try
            {
                string user = txtNome.Text;
                string senha = txtSenha.Text;

                DogMayque.Business.LoginBusiness business = new DogMayque.Business.LoginBusiness();
                DogMayque.Modelos.LoginModelo login = business.VerificarLogin(user, senha);

                if (login != null)
                {
                    DogMayque.Modelos.UserLogado.Id = login.Id;
                    DogMayque.Modelos.UserLogado.nome = login.User;

                    frmMenu tela = new frmMenu();
                    tela.Show();

                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Credenciais incorretas.", "DogMayque", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Desculpe, ocorreu um erro", "DogMayque", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);

            }

        }

        

        private void imgCadastrar_Click(object sender, EventArgs e)
        {
            frmCadastrarLogin tela = new frmCadastrarLogin();
            tela.Show();
            this.Hide();
        }
    }
}
